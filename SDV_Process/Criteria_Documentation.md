## **Documentation Criteria** ##

As one of the pillars of the SDV Process, the Documentation topic is dealt with in 3 stages and levels.

The **first stage** and level is addressed by simply ensuring the project repository contains a general README file that explains clearly what the project does and in which part of a typical SDV application / architecture it is supposed to be used.

The **second stage** and level is addressed by ensuring that the project repository contains:
- a general README file that explains clearly what the project does and in which part of a typical SDV application / architecture it is supposed to be used
- a Getting Started guide that explains how to use the project (how to build it, install it, use it, etc.)
- the necessary documentation about the external behaviour of the project (considering the project as a black box)


The **third stage** and level is addressed by ensuring that the project repository contains :
- a general README file that explains clearly what the project does and in which part of a typical SDV application / architecture it is supposed to be used
- a Getting Started guide that explains how to use the project (how to build it, install it, use it, etc.)
- the necessary documentation about the external behaviour of the project (considering the project as a black box)
- the necessary documentation on how to extend the project so that the extension fits all the practices that the project follows (e.g. adding new functionality, new requirements, with all that it implies, traceability, testing and test coverage, build instructions, CI/CD specifics that need to be kept / maintained)

