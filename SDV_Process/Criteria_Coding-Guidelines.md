## **Coding Guidelines Criteria** ##

As one of the pillars of the SDV Process, the Coding Guidelines topic is dealt with in 3 stages and levels.

The **first stage** and level is addressed by simply ensuring that the project has defined and adopted a set of coding guidelines. These Coding Guidelines of the project need to be properly documented and part of the project repository.

The **second stage** and level is addressed by ensuring that:
- the project has defined and adopted a set of coding guidelines, and has documented the guidelines as part of the project repository.
- the project has identified, adopted and established automatic methods to check out the appliance of the adopted coding guidelines, and that these methods are documented as part of the project repository.

The **third stage** and level is addressed by ensuring that:
- the project has defined and adopted a set of coding guidelines, and has documented the guidelines as part of the project repository.
- the project has identified, adopted and established automatic methods to check out the appliance of the adopted coding guidelines, and that these methods are documented as part of the project repository.
- the project reports any warnings / violations of its coding guidelines. Further on, the project team analyses the reported violations, documents the analysis, decided which violations to be fixed, and raises issues for those, and which violations to be justified through documentation.
