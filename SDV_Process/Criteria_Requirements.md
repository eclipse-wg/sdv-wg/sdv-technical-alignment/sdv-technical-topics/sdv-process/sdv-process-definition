## **Requirements Criteria** ##

As one of the pillars of the SDV Process, the Requirements topic is dealt with in 3 stages and levels.

The **first stage** and level is addressed by simply identifying and documenting the requirements of the project.

The **second stage** and level is addressed by:
- Identifying and documenting the requirements of the project.
- Ensuring that the project requirements are comprehensive and expose 100% of the project's features.
- Identifying, marking and tracking the changes made on the project requirements and specifying these in the release notes of the project.


The **third stage** and level is addressed by:
- Identifying and documenting the requirements of the project.
- Ensuring that the project requirements are comprehensive and expose 100% of the project's features.
- Identifying, marking and tracking the changes made on the project requirements and specifying these in the release notes of the project. 
- Referencing the requirements in the commits / PRs and in the test cases of the project.



