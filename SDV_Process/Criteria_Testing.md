## **Testing Criteria** ##


As one of the pillars of the SDV Process, the Testing topic is dealt with in 3 stages and levels.

The **first stage** and level is addressed by ensuring that:

- The project has a fully integrated CI/CD test pipeline 
- The project repository contains some dedicated tests


The **second stage** and level is addressed by ensuring that:

- The project has a fully integrated CI/CD test pipeline 
- The project repository contains some dedicated tests
- The project's tests are linked to the corresponding requirements 
- All project requirements are covered by tests


The **third stage** and level is addressed by ensuring that:

- The project has a fully integrated CI/CD test pipeline 
- The project repository contains some dedicated tests
- The project's tests are linked to the corresponding requirements 
- All project requirements are covered by tests
- The project's test coverage is equal to or higher than 80%
- There are no main-merges or releases of the project with failing tests


