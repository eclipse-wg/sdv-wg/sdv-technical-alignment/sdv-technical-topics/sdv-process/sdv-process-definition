## **Eclipse Foundation Software Defined Vehicles Process** ##

This document describes the Eclipse Foundation Software Defined Vehicles Process (EFSDVP) for optional use by the projects under the purview of the Eclipse Foundation Software Defined Vehicle Working Group.


The EFSDVP is concerned with building software that conforms with international best practices, norms and standards for software in general and software for the automotive industry in particular (e.g. A-SPICE) and moving releases of that software through evaluation to maturity levels. This process is concerned with establishing a baseline structure and set of practices.

The EFSDVP leverages and augments the Eclipse Foundation Development Process (EDP). The EDP defines important concepts, including the Open Source Rules of Engagement, the organisational framework for open source projects and teams, releases, reviews, and more.

This document, and future revisions thereof will be approved by the Eclipse Foundation Software Defined Vehicle Working Group Steering Committee.


