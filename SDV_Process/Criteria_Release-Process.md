## **Release Process Criteria** ##

As one of the pillars of the SDV Process, the Release Process topic is dealt with in 3 stages and levels.

The **first stage** and level is addressed by ensuring that the project repository contains:
- a well defined and documented Release Process for the project which includes clear release identifiers (e.g. tags in the repository that establish the contents of the release)
- Release Notes for each Release of the project, through which the project is documenting what the contents of the respective release are (all artifacts involved: code, documentation, requirements, tests, etc.) 

The **second stage** and level is addressed by ensuring that:
- the project repository contains a well defined and documented Release Process for the project which includes clear release identifiers (e.g. tags in the repository that establish the contents of the release)
- the project repository contains Release Notes for each Release of the project, through which the project is documenting what the contents of the respective release are (all artifacts involved: code, documentation, requirements, tests, etc.)  
- the project's release process is automated and also produces the Release Notes
- the project's releases are created according to the Eclipse Development Process

The **third stage** and level is addressed by  ensuring that:
- the project repository contains a well defined and documented Release Process for the project which includes clear release identifiers (e.g. tags in the repository that establish the contents of the release)
- the project repository contains Release Notes for each Release of the project, through which the project is documenting what the contents of the respective release are (all artifacts involved: code, documentation, requirements, tests, etc.)  
- the project's release process is automated and also produces the Release Notes
- the project's releases are created according to the Eclipse Development Process
- the project repository contains a published release schedule of the project
- the project's published release schedule is followed
- the project has successfully graduated from Incubation stage and follows semantic versioning

