## **Applicable Documents and Processes** ##

- Eclipse Foundation Bylaws
- Eclipse Foundation Development Process (EDP)
- Eclipse Foundation Working Group Process (EFWGP)
- Eclipse Foundation Intellectual Property Policy
- Eclipse Contributor Agreement
- Eclipse Contributor License Agreement


In the event of any conflict between the terms set forth in this document and the terms of the documents listed above, the terms of those documents shall take precedence.
