# Minutes from the "Safety Processes" break-out session
# Date: 06.07.2023 

# Talking points:

## KPIs, maturity model, badges
	- Badge lifecycle process (how to get new ones, sundown other ones)
	- we need productive, useful KPIs
	- Questions on Process:
		- What is it meant to achieve?
		- How to measure, how to get there?
			- Entry and exit criteria for a project - if pass, then Badge
	- Badges should be disjunct/disparate from each other (no dependencies)
	- Badges should be specific to be measurable and objective
		- "merge queue pattern applied"
		- "all project discussions, feature threads happen in the open"
		- "follows these best practices for merging"
		- "do all requirements have a test"
		- "did they write down all requirements?"
		- "certain level of documentation available?"
		- "we do have changelogs!"
		- "can deliver work products for X (eg. ASPICE Area Y)"
    - Badges could be of 2 types:
        - capability badges (e.g. the project is capable of producing and delivering X and Y artifacts)
        - fulfilment badges (e.g. the project is producing / delivering X and Y artifacts)

## Example/template/blueprints that make it easy for ppl to get to metrics
	- illustrate that there is an incremental path for projects to add capabilities/artifacts over time -> no up-front big bang required
	- start with something like testing in kuksa, as example for a certain category of quality assurance

## "Certifiability" - what do we need?
	- What are the smallest things that bring value?
	- ASPICE as a starting point
		- which subsets do make sense for us?
		- if we remove the commercial elements from ASPICE, we might end up being closer to ISO 26262 technical requirements
	- What levels to aim for?
		- what would make sense?
		- evaluate the projects we have to identify what would be most useful to start with
    - Evaluations against defined process
        - Ideally, we would be able to evaluate the appliance of the process (conformance) and, potentially, create some of the artifacts (defined through the process) by using some automated tools (those would also need to be "free" / open source)
	- Do not forget that next to focusing on process, it is also possible to argue from (consistently generated) results

## Other aspects
	- general maturity of project
		- "poc"
		- "production use"
		- "level of adoption"
		- "whatever"
	- do we really want to make binaries available?
		- might imply "fitness for purpose" that is misguided
		- range of possible target architectures, etc

## Do we do something (tooling) for conformance testing?
	- Idea: evolve joint/shared set of Gherkin test cases for similar kinds-of-projects
		- to be integrated and passed by every project in that category
		- -> one type of badge

## How do we proceed from here?
	- Johannes to present approaches from manufacturing
	- Where do we build?
		- GitHub Actions - are they enabled? Do they still need to be?
		- GitLab Actions? ...
		- Eclipse Foundation to pursue

# Starting point ideas:
	- Requirements process
		- Requirements are available
		- There are tests for requirements
		- They are linked to requirement
		- They are run automatically
		- They pass (there is no release with failing tests)
	- Release process
		- There are release notes containing relevant info
	- Document all of this properly

