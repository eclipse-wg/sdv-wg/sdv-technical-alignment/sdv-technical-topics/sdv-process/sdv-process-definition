# Eclipse Foundation Software Defined Vehicles Process Community Contributing Guide

This project contains the source for the Eclipse Foundation Software Defined Vehicles Process in AsciiDoc format, the build script, and generated output.

## Terms of Use

This repository is subject to the Terms of Use of the Eclipse Foundation

* https://www.eclipse.org/legal/termsofuse.php

## Update Process

Please submit pull requests for any changes. We’ll use pull requests for discussing them.

New requirements, ideas and significant changes should be captured in issues. Discussion summary / results must be documented (and referenced) in pull requests and issues.

### Eclipse Contributor Agreement

Before your contribution can be accepted by the project team, contributors must
electronically sign the Eclipse Contributor Agreement (ECA).

* http://www.eclipse.org/legal/ECA.php

Commits that are provided by non-committers must have a Signed-off-by field in
the footer indicating that the author is aware of the terms by which the
contribution has been provided to the project. The non-committer must
additionally have an Eclipse Foundation account and must have a signed Eclipse
Contributor Agreement (ECA) on file.

For more information, please see the Eclipse Committer Handbook:
https://www.eclipse.org/projects/handbook/#resources-commit


