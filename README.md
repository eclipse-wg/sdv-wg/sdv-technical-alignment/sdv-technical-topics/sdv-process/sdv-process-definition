# sdv-process-definition

This project is dedicated to the Eclipse Foundation Software Defined Vehicles Process (EFSDVP), which is focused on content quality for the Eclipse SDV Projects. 

The EFSDVP is not mandatory. 
It is for each Eclipse SDV project to decide whether to apply the EF SDV process or not. 

Once a project starts applying the EF SDV Process, it will implicitly be evaluated against the EFSDVP and, consequently, will earn maturity badges as per the results of the evaluations.

